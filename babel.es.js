module.exports = {
  presets: ['@babel/preset-typescript'],
  targets: {
    esmodules: true,
  },
};
