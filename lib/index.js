"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _excluded = ["keyPath", "indexs"],
    _excluded2 = ["name"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class IndexdbHelper {
  constructor(databaseName, tbConfigs, dbVersion) {
    _defineProperty(this, "haveIndex", false);

    _defineProperty(this, "canuseIndex", false);

    _defineProperty(this, "tbConfigs", []);

    this.databaseName = databaseName;
    this.dbVersion = dbVersion || 2;
    this.tbConfigs = tbConfigs || [];

    if (window.indexedDB) {
      this.haveIndex = true;
    } else this.haveIndex = false;
  } // indexdb 初始化


  ready() {
    var _this = this;

    if (this.haveIndex) {
      if (this.db) return Promise.resolve(this.db);
      return new Promise( /*#__PURE__*/function () {
        var _ref = _asyncToGenerator(function* (resolve, reject) {
          var request = window.indexedDB.open(_this.databaseName, _this.dbVersion);

          request.onerror = () => {
            console.log("数据库打开报错");
            _this.canuseIndex = false;
            reject();
          };

          request.onsuccess = /*#__PURE__*/_asyncToGenerator(function* () {
            _this.db = request.result;
            console.log("数据库打开成功");
            _this.canuseIndex = true;
            resolve(_this.db);
          });

          request.onupgradeneeded = event => {
            _this.canuseIndex = true;
            _this.db = event.target.result;

            _this.initTb();

            var transaction = event.target.transaction;

            transaction.oncomplete = function () {
              resolve(this.db);
            };
          };
        });

        return function (_x, _x2) {
          return _ref.apply(this, arguments);
        };
      }());
    }

    return Promise.resolve(null);
  } // 创建表


  createStore(tbName, tconfig) {
    var _this$db;

    if (!this.canuseIndex || !this.haveIndex) return;
    var objectStore;

    if (!((_this$db = this.db) !== null && _this$db !== void 0 && _this$db.objectStoreNames.contains(tbName))) {
      var _this$db2;

      var _ref3 = tconfig || {},
          {
        keyPath,
        indexs = []
      } = _ref3,
          config = _objectWithoutProperties(_ref3, _excluded);

      var key = keyPath;
      var indexList = indexs;

      if (!key) {
        key = "id";
        config.autoIncrement = true;

        if (!indexList.length) {
          indexList = [{
            name: "id",
            prop: "id",
            config: {
              unique: true
            }
          }];
        }
      }

      objectStore = (_this$db2 = this.db) === null || _this$db2 === void 0 ? void 0 : _this$db2.createObjectStore(tbName, _objectSpread(_objectSpread({}, config), {}, {
        keyPath: key
      }));
      indexList.forEach(index => this.createIndex(objectStore, index));
    }

    return objectStore;
  } // 创建index


  createIndex(tb, index) {
    if (!tb || !this.db) return false;
    var sotre;

    if (typeof tb === "string") {
      sotre = this.db.transaction(tb).objectStore(tb);
    } else {
      sotre = tb;
    }

    var {
      name,
      prop,
      config
    } = index;
    sotre.createIndex(name, prop, config);
    return true;
  } // 删除表中所有数据


  dropStore(tbName) {
    return new Promise((resolve, reject) => {
      if (!this.canuseIndex || !this.db) return reject();
      var objectStore = this.db.transaction(tbName).objectStore(tbName);
      var request = objectStore.openCursor();

      request.onsuccess = function (event) {
        var cursor = event.target.result;

        if (cursor) {
          cursor.delete();
        } else {
          resolve(true);
        }
      };

      request.onerror = function () {
        reject();
      };
    });
  } // 插入数据


  create(tbName, data) {
    var _this2 = this;

    return new Promise( /*#__PURE__*/function () {
      var _ref4 = _asyncToGenerator(function* (resolve, reject) {
        if (!_this2.canuseIndex || !_this2.db) return reject();

        var keyPath = _this2.db.transaction([tbName], "readwrite").objectStore(tbName).keyPath;

        var isExist = yield _this2.findByPk(tbName, data[keyPath]);
        if (isExist) resolve(true);

        var request = _this2.db.transaction([tbName], "readwrite").objectStore(tbName).add(data);

        request.onsuccess = function () {
          resolve(data);
        };

        request.onerror = function () {
          reject();
        };
      });

      return function (_x3, _x4) {
        return _ref4.apply(this, arguments);
      };
    }());
  }

  bulkCreate(tbName, data) {
    var _this3 = this;

    return new Promise( /*#__PURE__*/function () {
      var _ref5 = _asyncToGenerator(function* (resolve, reject) {
        if (!_this3.canuseIndex || !_this3.db) return reject();
        if (!Array.isArray(data) || data.length === 0) return resolve(true);
        var retList = yield Promise.all(data.map( /*#__PURE__*/function () {
          var _ref6 = _asyncToGenerator(function* (d) {
            return yield _this3.create(tbName, d);
          });

          return function (_x7) {
            return _ref6.apply(this, arguments);
          };
        }()));
        if (retList.every(t => !!t)) resolve(true);else reject();
      });

      return function (_x5, _x6) {
        return _ref5.apply(this, arguments);
      };
    }());
  } // 根据主键查找


  findByPk(tbName, keyPath) {
    return new Promise((resolve, reject) => {
      if (!this.canuseIndex || !this.db) return reject();
      var transaction = this.db.transaction([tbName]);
      var objectStore = transaction.objectStore(tbName);
      var request = objectStore.get(keyPath);

      request.onerror = function () {
        reject();
      };

      request.onsuccess = function () {
        if (request.result) {
          resolve(request.result);
        } else {
          resolve(null);
        }
      };
    });
  } // 根据索引查找


  findByIndex(tbName, indexName, keyPath) {
    return new Promise((resolve, reject) => {
      if (!this.canuseIndex || !this.db) return reject();
      var request = this.db.transaction([tbName]).objectStore(tbName).index(indexName).get(keyPath);

      request.onerror = function () {
        reject();
      };

      request.onsuccess = function (e) {
        var result = e.target.result;

        if (result) {
          resolve(result);
        } else {
          resolve(null);
        }
      };
    });
  } //   遍历指定表


  query(tbName, filter) {
    return new Promise((resolve, reject) => {
      var list = [];
      if (!this.canuseIndex || !this.db) return reject();
      var objectStore = this.db.transaction(tbName).objectStore(tbName);
      var request = objectStore.openCursor();

      request.onsuccess = function (event) {
        var cursor = event.target.result;

        if (cursor) {
          if (!filter || filter(cursor.value)) list.push(cursor.value);
          cursor.continue();
        } else {
          resolve(list);
        }
      };

      request.onerror = function () {
        reject();
      };
    });
  } //   遍历指定表


  forEach(tbName, callback) {
    return new Promise((resolve, reject) => {
      if (!this.canuseIndex || !this.db) return reject();
      var objectStore = this.db.transaction(tbName).objectStore(tbName);
      var request = objectStore.openCursor();

      request.onsuccess = function (event) {
        var cursor = event.target.result;

        if (cursor) {
          callback(cursor);
        } else {
          resolve(true);
        }
      };

      request.onerror = function () {
        reject();
      };
    });
  } //   更新指定数据


  update(tbName, data) {
    return new Promise((resolve, reject) => {
      if (!this.canuseIndex || !this.db) return reject();
      var request = this.db.transaction([tbName], "readwrite").objectStore(tbName).put(data);

      request.onsuccess = function () {
        resolve(data);
      };

      request.onerror = function () {
        reject();
      };
    });
  } // 删除数据


  delete(tbName, keyPath) {
    return new Promise((resolve, reject) => {
      if (!this.canuseIndex || !this.db) return reject();
      var request = this.db.transaction([tbName], "readwrite").objectStore(tbName).delete(keyPath);

      request.onsuccess = function () {
        resolve(true);
      };

      request.onerror = function (e) {
        reject(e);
      };
    });
  }

  initTb() {
    this.tbConfigs.forEach(_ref7 => {
      var {
        name
      } = _ref7,
          config = _objectWithoutProperties(_ref7, _excluded2);

      this.createStore(name, config);
    });
  }

}

var _default = IndexdbHelper;
exports.default = _default;