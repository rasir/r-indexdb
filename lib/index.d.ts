interface TbConfig {
    name: string;
    autoIncrement?: boolean;
    keyPath?: string;
    indexs?: IndexConfig[];
}
interface IndexConfig {
    name: string;
    prop: string;
    config?: {
        unique: boolean;
    };
}
declare class IndexdbHelper {
    db?: IDBDatabase;
    haveIndex: boolean;
    canuseIndex: boolean;
    databaseName?: string;
    dbVersion?: number;
    tbConfigs: TbConfig[];
    constructor(databaseName: string, tbConfigs?: TbConfig[], dbVersion?: number);
    ready(): Promise<IDBDatabase | null>;
    createStore(tbName: string, tconfig?: Omit<TbConfig, "name">): IDBObjectStore | undefined;
    createIndex(tb: string | IDBObjectStore | undefined, index: IndexConfig): boolean;
    dropStore(tbName: string): Promise<unknown>;
    create(tbName: string, data: any): Promise<unknown>;
    bulkCreate(tbName: string, data: any[]): Promise<unknown>;
    findByPk(tbName: string, keyPath: string): Promise<unknown>;
    findByIndex(tbName: string, indexName: string, keyPath: string): Promise<unknown>;
    query(tbName: string, filter?: (item: any) => boolean): Promise<unknown>;
    forEach(tbName: string, callback: (cursor: any) => void): Promise<unknown>;
    update(tbName: string, data: any): Promise<unknown>;
    delete(tbName: string, keyPath: string): Promise<unknown>;
    initTb(): void;
}
export default IndexdbHelper;
